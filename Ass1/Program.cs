﻿// See https://aka.ms/new-console-template for more information
class NguoiLaoDong
{
	public string HoTen { get; set; }
	public int NamSinh { get; set; }	

	public double LuongCoBan { get; set; }

	public NguoiLaoDong() { }

	public NguoiLaoDong(string hoTen, int namSinh,double luongCoBan) {
		HoTen = hoTen;
		NamSinh = namSinh;
		LuongCoBan = luongCoBan;
	}

	public void NhapThongTin(string hoTen, int namSinh, double luongCoBan) {
		HoTen = hoTen;
		NamSinh = namSinh;
		LuongCoBan = luongCoBan;
	}

	public virtual double TinhLuong()
	{
		return LuongCoBan;
	}
	public virtual void XuatThongTin()
	{
		Console.WriteLine("Ho ten la:" + HoTen + "namsinh"+NamSinh+"Luong co ban"+LuongCoBan);
	}

}

class GiaoVien : NguoiLaoDong {
	private double HeSoLuong {get; set;}

	public GiaoVien() { }

    public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong) : base(hoTen, namSinh, luongCoBan)
	{
		HeSoLuong = heSoLuong;
	} 

	public void nhapThongTin(double heSoLuong)
	{
		HeSoLuong = heSoLuong;
	}

	public override double TinhLuong()
	{
		return HeSoLuong * 1.25 * LuongCoBan;
	}

	public override void XuatThongTin()
	{
		base.XuatThongTin();
		Console.WriteLine("HesoLuong" + HeSoLuong + "Luong" + TinhLuong());
	}

	public void XuLi()
	{
		HeSoLuong += 0.6;
	}
}

class Program
{
	static void Main(string[] args)
	{
		Console.Write("Nhap so luong giao vien: ");
		int soLuong = int.Parse(Console.ReadLine());

		List<GiaoVien> danhSachGiaoVien = new List<GiaoVien>();

		for (int i = 0; i < soLuong; i++)
		{
			Console.WriteLine($"Nhap thong tin giao vien {i + 1}:");
			Console.Write("Ho ten: ");
			string hoTen = Console.ReadLine();
			Console.Write("Nam sinh: ");
			int namSinh = int.Parse(Console.ReadLine());
			Console.Write("Luong co ban: ");
			double luongCoBan = double.Parse(Console.ReadLine());
			Console.Write("He so luong: ");
			double heSoLuong = double.Parse(Console.ReadLine());

			GiaoVien giaoVien = new GiaoVien(hoTen, namSinh, luongCoBan, heSoLuong);
			danhSachGiaoVien.Add(giaoVien);
			Console.WriteLine();
		}

		double luongThapNhat = double.MaxValue;
		GiaoVien giaoVienLuongThapNhat = null;

		foreach (GiaoVien giaoVien in danhSachGiaoVien)
		{
			if (giaoVien.TinhLuong() < luongThapNhat)
			{
				luongThapNhat = giaoVien.TinhLuong();
				giaoVienLuongThapNhat = giaoVien;
			}
		}

		Console.WriteLine("Thong tin giao vien co luong thap nhat:");
		giaoVienLuongThapNhat.XuatThongTin();

		Console.WriteLine("Nhan phim bat ky de thoat...");
		Console.ReadKey();
	}
}

